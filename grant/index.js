'use strict';


const EDSU_GRANT_PROTOCOL_VERSION = '0.1';
const EDSU_GRANT_CONNECT_TRY_INTERVAL_MS = 256;
const EDSU_GRANT_ERR_INVALID_INPUT = 'invalid-input';
const EDSU_GRANT_ERR_EDSU_SERVER = 'edsu-server';
const EDSU_GRANT_OP_ERR = 'err';
const EDSU_GRANT_OP_HELLO = 'hello';
const EDSU_GRANT_OP_GRANT_REQUEST = 'grant-request';
const EDSU_GRANT_OP_EDSU_GRANT_REQUEST_RESULT = 'grant-request-result';
const EDSU_GRANT_REQUEST_GRANTED = 'granted';
const EDSU_GRANT_REQUEST_REFUSED = 'refused';
const EDSU_GRANT_REQUEST_REFUSED_BY_HOST = 'refused-by-host';


const EdsuGrantError = {
    refused: Symbol('grant request refused'),
    refusedByHost: Symbol('grant request refused by host'),
    invalidInput: Symbol('invalid input'),
    comms: Symbol('communication with the grantor tab has failed'),
    edsuComms: Symbol('communication with the user\'s Edsu server has failed'),
}

let EdsuGrantDebug = false;

// request: capabilities (.owner and/or .visitor), label, explanation, durationSeconds, once
// cbs: err, granted
function edsuGrantRequest(username, request, cbs) {
    // Basic validation
    let usernameParts = /^([a-z0-9-]+)@([a-z0-9-]+\.[a-z0-9-]+$)/.exec(username);
    if (!usernameParts) {
        _edsuGrantWrapCb(cbs.err)({
            err: EdsuGrantError.invalidInput,
            debug: 'invalid username',
        });
        return;
    }
    if (request.durationSeconds !== undefined && !Number.isInteger(request.durationSeconds)) {
        _edsuGrantWrapCb(cbs.err)({
            err: EdsuGrantError.invalidInput,
            debug: 'request.durationSeconds must either be an integer, or not present',
        });
        return;
    }

    // Set up some useful state
    let grantorOrigin = 'https://' + usernameParts[1] + '.edsu.' + usernameParts[2];;
    let url = grantorOrigin + '/edsu/grant/';
    let state = null;
    let connectInterval = null;
    let receiver;
    let tab = window.open(url);

    let die = (err, debug) => {
        done();
        _edsuGrantWrapCb(cbs.err)({ err, debug });
    };
    let done = () => {
        window.removeEventListener('message', receiver);
        state = 'done';
        if (connectInterval !== null) {
            clearInterval(connectInterval);
            connectInterval = null;
        }
        if (!EdsuGrantDebug)
            tab.close();
    };

    // Set up receiver
    receiver = e => {
        let msg = e.data;
        if (e.origin != grantorOrigin) {
            // Message isn't for us
            return;
        }

        // Handle error
        if (msg.op == EDSU_GRANT_OP_ERR) {
            if (msg.err == EDSU_GRANT_ERR_INVALID_INPUT) {
                die(EdsuGrantError.invalidInput, msg.debug);
            } else if (msg.err == EDSU_GRANT_ERR_EDSU_SERVER) {
                die(EdsuGrantError.edsuComms, msg.debug);
            } else {
                die(EdsuGrantError.comms, msg.debug);
            }
            return;
        }

        switch (state) {
            case 'connecting': {
                if (msg.op != EDSU_GRANT_OP_HELLO) {
                    die(EdsuGrantError.comms, 'unexpected message from the grantor: ' + msg.op);
                } else {
                    state = 'waiting';
                    if (connectInterval !== null)
                        clearInterval(connectInterval);
                    connectInterval = null;

                    // Send request
                    tab.postMessage({
                        op: EDSU_GRANT_OP_GRANT_REQUEST,
                        capabilitiesOwner: request.capabilitiesOwner,
                        capabilitiesVisitor: request.capabilitiesVisitor,
                        explanation: request.explanation,
                        label: request.label,
                        once: request.once,
                        duration: request.duration,
                    }, grantorOrigin);
                    state = 'requested';
                }
            }; break;
            case 'requested': {
                if (msg.op != EDSU_GRANT_OP_EDSU_GRANT_REQUEST_RESULT) {
                    die(EdsuGrantError.comms, 'unexpected message from the grantor: ' + msg.op);
                    return;
                }
                if (msg.result == EDSU_GRANT_REQUEST_REFUSED) {
                    die(EdsuGrantError.refused, 'request refused');
                } else if (msg.result == EDSU_GRANT_REQUEST_REFUSED_BY_HOST) {
                    die(EdsuGrantError.refusedByHost, 'request refused by host');
                } else if (msg.result == EDSU_GRANT_REQUEST_GRANTED) {
                        let arg = {};
                        for (let k of ['tokenOwner', 'tokenVisitor', 'expires', 'once']) {
                            if (msg[k] !== undefined)
                            arg[k] = msg[k];
                        }
                        _edsuGrantWrapCb(cbs.granted)(arg);
                        done();
                } else {
                    die(EdsuGrantError.comms, 'unexpected grant result from the grantor: ' +
                                              JSON.stringify(msg));
                    return;
                }
            }; break;
            default: {
                die(EdsuGrantError.comms, 'unexpected message from the grantor: ' + msg.op);
                return;
            }
        }
    }
    window.addEventListener('message', receiver);

    // Repeatedly send hello until we're connected
    state = 'connecting';
    connectInterval = setInterval(() => {
        tab.postMessage({ 
            op: 'hello',
            versions: [EDSU_GRANT_PROTOCOL_VERSION],
        }, grantorOrigin);
    }, EDSU_GRANT_CONNECT_TRY_INTERVAL_MS);

    // Monitor the tab for closing too
    //tab.addEventListener('beforeunload', () => {
        //die(EdsuGrantError.comms, 'grantor tab was closed');
        // TODO: this was being fired immediately on Safari - look into
        //       further - even adding the event listener causes a problem
        //       with Edge
    //});
}

// Same args as edsuGrantRequest
function edsuAsyncGrantRequest(username, request) {
    return new Promise((granted, err) => {
        edsuGrantRequest(username, request, { granted, err });
    });
}

// cb: callback when return/button is pressed
function edsuUsernameElement(cb) {
    let wrappedCb = _edsuGrantWrapCb(cb);
    let container = document.createElement('div');
    container.innerHTML = `<div style="border: 2px solid rgba(0, 0, 0, 0.4); padding: 5px 5px;
        display: inline-flex; width: 338px; justify-content: space-between;
        background-color: rgba(255, 255, 255, 0.85); border-radius: 3px 3px;">
        <a style="border: none; text-decoration: none; margin: 0; padding: 0;"
        href="https://edsu.org/what-is-an-edsu-app/"><svg style="vertical-align: bottom"
        width="34" height="38" viewBox="0 0 34 38" xmlns="http://www.w3.org/2000/svg">
        <path fill="#444" d="m0 0v38h34v-38zm6 2h10v4h-10v2h6v4h-6v2h10v4h-10a4 4 0 0 1-4-4v-8a4
        4 0 0 1 4-4zm12 0h10a4 4 0 0 1 4 4v8a4 4 0 0 1-4 4h-10v-12zm4 4v8h6v-8zm-16
        14h10v4h-10v2h6a4 4 0 0 1 4 4v2a4 4 0 0 1-4 4h-10v-4h10v-2h-6a4 4 0 0 1-4-4v-2a4 4 0 0 1
        4-4zm12 0h4v12h6v-12h4v12a4 4 0 0 1-4 4h-6a4 4 0 0 1-4-4z"></svg></a><input type="email"
        placeholder="you@example.com" style="width: 243px; padding: 0 5px; box-sizing: border-box;
        border-radius: 5px 5px; border: 1px solid rgba(0, 0, 0, 0.2)"><button style="cursor:
        pointer; width: 50px; padding: 0 0; border: 1px solid #aaa; border-radius: 5px 5px;
        background-color: rgba(200, 200, 200, 0.5);" onMouseOver="this.style.backgroundColor=
        'rgba(255, 255, 255, 0.5)'" onMouseLeave="this.style.backgroundColor=
        'rgba(200, 200, 200, 0.5)'">&rarr;</button></div>`;
    let div = container.querySelector('div');
    let input = div.querySelector('input');
    div.querySelector('button').addEventListener('click', () => wrappedCb(input.value.trim()));
    input.addEventListener('keypress', e => {
        if (e.keyCode == 13 || e.which == 13)
            wrappedCb(input.value.trim());
    });
    return div;
}


function _edsuGrantWrapCb(f) {
    function ff() {
        if (!f)
            return;
        try {
            f.apply(null, arguments);
        } catch (e) {
            (typeof EdsuLog != 'undefined' ? EdsuLog : console.error)(
                'caught exception in callback:', e);
        }
    }
    return ff;
}
