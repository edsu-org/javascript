![Edsu](https://gitlab.com/edsu-org/rust/raw/master/logo.png)

# Edsu Javascript

This is the official [edsu.org](https://edsu.org/) repo for any Javascript code related to Edsu.  To use these libraries you'll most likely want to include them from the Edsu CDN (see each library's docs for links).  But you can also just download them from here and use them if you'd like - there are no external dependencies.

These will be put in the NPM registry once they've stabilized a little.

## Contents

### raw/

Docs: http://su.edclave.ca/resources/javascript/edsu-raw/

A complete implementation of the client protocol, as well as fundemental functionality such as automatic reconnects and retries.

### grant/

Docs: http://su.edclave.ca/resources/javascript/edsu-grant/

A complete implementation of the inter-browser-tab capabilities granting protocol.  Use this to acquire a capabilities token for use with Edsu connections.

### storage-basic/

Docs: http://su.edclave.ca/resources/javascript/edsu-storage-basic/

A convenience layer above raw, for when all you want to do with Edsu is store and retrieve data.

### basic/

An incomplete layer above raw, adding async and convenience functions.  Currently in-flux and likely to change - have a look if you're curious, but pardon the dust.


## Props

Testing support graciously provided by:

[![BrowserStack logo](https://gitlab.com/edsu-org/javascript/raw/master/browserstack/logo.png)](https://www.browserstack.com/)
